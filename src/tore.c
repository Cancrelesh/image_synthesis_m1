/*=================================================================*/
/*= E.Incerti - eric.incerti@univ-eiffel.fr                       =*/
/*= Université Gustave Eiffel                                     =*/
/*= Code "squelette" pour prototypage avec libg3x.6c              =*/
/*=================================================================*/

#include <g3x.h>

/* tailles de la fenêtre (en pixel) */
static int WWIDTH = 512, WHEIGHT = 512;

/* global */
G3Xpoint *vertex_coord;
G3Xpoint *normal_coord;

const double rc2 = 0.3;
const unsigned int NBM = 80;
const unsigned int NBP = 80;
const unsigned int vertex_number = NBM * NBP;

const double tetam = 2 * M_PI / NBM;
const double phip = M_PI / (NBP - 1);

const GLfloat sz_pt = 5;

/* functions signatures */
void my_tore(G3Xpoint *vertex_coord, G3Xpoint *normal_coord);
void free_my_tore(G3Xpoint *vertex_coord, G3Xpoint *normal_coord);
void display_points_my_tore(G3Xpoint *vertex_coord, G3Xpoint *normal_coord);

/* la fonction d'initialisation : appelée 1 seule fois, au début */
static void init(void)
{
    vertex_coord = (G3Xpoint *)calloc(sizeof(G3Xpoint), vertex_number);
    normal_coord = (G3Xpoint *)calloc(sizeof(G3Xpoint), vertex_number);

    my_tore(vertex_coord, normal_coord);
}

/* la fonction de contrôle : appelée 1 seule fois, juste après <init> */
static void ctrl(void)
{
}

/* la fonction de dessin : appelée en boucle */
static void draw(void)
{
    display_points_my_tore(vertex_coord, normal_coord);
}

/* la fonction d'animation (facultatif) */
static void anim(void)
{
}

/* la fonction de sortie  (facultatif) -- atexit() */
static void quit(void)
{
    free_my_tore(vertex_coord, normal_coord);
}

/***************************************************************************/
/* La fonction principale : NE CHANGE JAMAIS ou presque                    */
/***************************************************************************/
int main(int argc, char **argv)
{
    /* creation de la fenetre - titre et tailles (pixels) */
    g3x_InitWindow(*argv, WWIDTH, WHEIGHT);

    g3x_SetInitFunction(init); /* fonction d'initialisation */
    g3x_SetCtrlFunction(ctrl); /* fonction de contrôle      */
    g3x_SetDrawFunction(draw); /* fonction de dessin        */
    g3x_SetAnimFunction(anim); /* fonction d'animation      */
    g3x_SetExitFunction(quit); /* fonction de sortie        */

    /* lancement de la boucle principale */
    return g3x_MainStart();
    /* RIEN APRES CA */
}

void my_tore(G3Xpoint *vertex_coord, G3Xpoint *normal_coord)
{
    double px, py, pz;
    double x_angc2, y_angc2;

    for (int p = 0; p < NBP; p++)
    {
        x_angc2 = rc2 * cos(p * tetam);
        y_angc2 = rc2 * sin(p * tetam);
        for (int m = 0; m < NBM; m++)
        {
            px = (1 + x_angc2) * cos(m * tetam);
            py = -(1 + x_angc2) * sin(m * tetam);
            pz = y_angc2;

            vertex_coord[p * NBM + m] = g3x_Point(px, py, pz);
            normal_coord[p * NBM + m] = g3x_Point(px, py, pz);
        }
    }
}

void free_my_tore(G3Xpoint *vertex_coord, G3Xpoint *normal_coord)
{

    free(vertex_coord);
    free(normal_coord);
}

void display_points_my_tore(G3Xpoint *vertex_coord, G3Xpoint *normal_coord)
{
    glPointSize(sz_pt);
    g3x_Material(G3Xo, 0.5, 0.7, 0.3, 35, 0.32);
    glBegin(GL_POINTS);
    for (int i = 0; i < vertex_number; i++)
    {
        g3x_Normal3dv(normal_coord[i]);
        g3x_Vertex3dv(vertex_coord[i]);
    }
    glEnd();
}