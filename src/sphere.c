/*=================================================================*/
/*= E.Incerti - eric.incerti@univ-eiffel.fr                       =*/
/*= Université Gustave Eiffel                                     =*/
/*= Code "squelette" pour prototypage avec libg3x.6c              =*/
/*=================================================================*/

#include <g3x.h>

/* tailles de la fenêtre (en pixel) */
static int WWIDTH = 512, WHEIGHT = 512;

/* global */
G3Xpoint *vertex_coord;
G3Xpoint *normal_coord;

const unsigned int NBM = 1000;
const unsigned int NBP = 1000;
const unsigned int vertex_number = NBM * NBP;
const unsigned int facets_number = NBM * 2 * NBP * 2;

const double tetam = 2 * M_PI / NBM;
const double phip = M_PI / (NBP - 1);

const GLfloat sz_pt = 5;

/* functions signatures */
void my_sphere(G3Xpoint *vertex_coord, G3Xpoint *normal_coord);
void free_my_sphere(G3Xpoint *vertex_coord, G3Xpoint *normal_coord);
void display_points_my_sphere(const G3Xpoint *vertex_coord, const G3Xpoint *normal_coord);
void display_triangles_my_sphere(const G3Xpoint *vertex_coord, const G3Xpoint *normal_coord);

/* la fonction d'initialisation : appelée 1 seule fois, au début */
static void init(void)
{
    vertex_coord = (G3Xpoint *)calloc(sizeof(G3Xpoint), vertex_number);
    normal_coord = (G3Xpoint *)calloc(sizeof(G3Xpoint), vertex_number);

    my_sphere(vertex_coord, normal_coord);
}

/* la fonction de contrôle : appelée 1 seule fois, juste après <init> */
static void ctrl(void)
{
}

/* la fonction de dessin : appelée en boucle */
static void draw(void)
{
    //display_points_my_sphere(vertex_coord, normal_coord);
    display_triangles_my_sphere(vertex_coord, normal_coord);
}

/* la fonction d'animation (facultatif) */
static void anim(void)
{
}

/* la fonction de sortie  (facultatif) -- atexit() */
static void quit(void)
{
    free_my_sphere(vertex_coord, normal_coord);
}

/***************************************************************************/
/* La fonction principale : NE CHANGE JAMAIS ou presque                    */
/***************************************************************************/
int main(int argc, char **argv)
{
    /* creation de la fenetre - titre et tailles (pixels) */
    g3x_InitWindow(*argv, WWIDTH, WHEIGHT);

    g3x_SetInitFunction(init); /* fonction d'initialisation */
    g3x_SetCtrlFunction(ctrl); /* fonction de contrôle      */
    g3x_SetDrawFunction(draw); /* fonction de dessin        */
    g3x_SetAnimFunction(anim); /* fonction d'animation      */
    g3x_SetExitFunction(quit); /* fonction de sortie        */

    /* lancement de la boucle principale */
    return g3x_MainStart();
    /* RIEN APRES CA */
}

void my_sphere(G3Xpoint *vertex_coord, G3Xpoint *normal_coord)
{
    double z_ang, y_ang, px, py, pz;

    for (int p = 0; p < NBP; p++)
    {
        z_ang = cos(p * phip);
        y_ang = sin(p * phip);
        for (int m = 0; m < NBM; m++)
        {
            px = y_ang * cos(m * tetam);
            py = y_ang * sin(m * tetam);
            pz = z_ang;

            vertex_coord[p * NBM + m] = g3x_Point(px, py, pz);
            normal_coord[p * NBM + m] = g3x_Point(px, py, pz);
        }
    }
}

void free_my_sphere(G3Xpoint *vertex_coord, G3Xpoint *normal_coord)
{

    free(vertex_coord);
    free(normal_coord);
}

void display_points_my_sphere(const G3Xpoint *vertex_coord, const G3Xpoint *normal_coord)
{
    glPointSize(sz_pt);
    g3x_Material(G3Xo, 0.5, 0.7, 0.3, 35, 0.32);
    glBegin(GL_POINTS);
    for (int i = 0; i < vertex_number; i++)
    {
        g3x_Normal3dv(normal_coord[i]);
        g3x_Vertex3dv(vertex_coord[i]);
    }
    glEnd();
}

void display_triangles_my_sphere(const G3Xpoint *vertex_coord, const G3Xpoint *normal_coord)
{
    g3x_Material(G3Xo, 0.5, 0.7, 0.3, 35, 0.32);
    glBegin(GL_TRIANGLES);
    
    enum state {
        R, // right of the matrix
        B, // bottom of the matrix
        C, // corner of the matrix
        D // position without concequences (default)
    };

    enum state pos;
    G3Xpoint a;
    G3Xpoint b;
    G3Xpoint c;
    G3Xpoint b2;
        
    for(int i = 0; i < NBP; i++) {
        for(int j = 0; j < NBM; j++) {
            
            pos = D;

            if (j == NBM - 1 && i < NBP - 1) {
                pos = R;
            }
            if (j < NBM - 1 && i == NBP - 1) {
                pos = B;
            }
            if (j == NBM - 1 && i == NBP - 1) {
                pos = C;
            }

            switch (pos) {

                case R:
                    b = vertex_coord[j + ((i + 1) * NBM)];
                    c = vertex_coord[((i + 1) * NBM)];
                    b2 = vertex_coord[i * NBM];
                    break;
                
                case B:
                    continue;
                //     b = vertex_coord[j];
                //     c = vertex_coord[j + 1];
                //     b2 = vertex_coord[(j + 1) + (i * NBM)];
                //     break;
                
                case C:
                    continue;
                //     b = vertex_coord[j];
                //     c = vertex_coord[0];
                //     b2 = vertex_coord[i * NBM];
                //     break;

                default:
                    b = vertex_coord[j + ((i + 1) * NBM)];
                    c = vertex_coord[j + ((i + 1) * NBM) + 1];
                    b2 = vertex_coord[j + (i * NBM) + 1];
                    break;
            }

            a = vertex_coord[j + (i * NBM)];
            
            g3x_Normal3dv(normal_coord[j + ((i + 1) * NBM)]);
            g3x_Vertex3dv(a);
            g3x_Vertex3dv(b);
            g3x_Vertex3dv(c);

            g3x_Normal3dv(normal_coord[j + (i * NBM) + 1]);
            g3x_Vertex3dv(a);
            g3x_Vertex3dv(b2);
            g3x_Vertex3dv(c);
        }
    }
    glEnd();
}